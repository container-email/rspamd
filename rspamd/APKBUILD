# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Nathan Angelacos <nangel@alpinelinux.org>
# Contributor: TBK <alpine@jjtc.eu>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=rspamd
pkgver=3.6_git020423
pkgrel=0
# allow for test builds of commits (update pkgver date when changing)
_commit=f62b23fdebe3a0630121f1e515a5e2846b8ef827
pkgdesc="Fast, free and open-source spam filtering system"
url="https://rspamd.com/"
arch="all"
license="Apache-2.0 BSD-1-Clause BSD-2-Clause BSD-3-Clause CC0-1.0 LGPL-2.1-or-later
	LGPL-3.0-only MIT Zlib"
pkgusers="rspamd"
pkggroups="rspamd"
depends="icu-data-full"
makedepends="
	cmake
	clang
	curl-dev
	fmt-dev
	glib-dev
	icu-dev
	libsodium-dev
	libstemmer-dev
	openssl-dev>3
	pcre2-dev
	perl
	ragel
	samurai
	sqlite-dev
	xxhash-dev
	zlib-dev
	zstd-dev
	"
case "$CARCH" in
	riscv64|x86) makedepends="$makedepends lua5.4-dev"
		_luajit="OFF" ;;
	*) makedepends="$makedepends luajit-dev"
		_luajit="ON" ;;
esac
install="$pkgname.pre-install $pkgname.post-upgrade"
subpackages="
	$pkgname-dbg
	$pkgname-doc
	$pkgname-client
	$pkgname-libs
	$pkgname-utils::noarch
	$pkgname-controller::noarch
	$pkgname-fuzzy::noarch
	$pkgname-proxy::noarch
	$pkgname-openrc
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/rspamd/rspamd/archive/${_commit:-$pkgver}.tar.gz
	$pkgname.logrotated
	$pkgname.initd
	$pkgname.confd
	10-control_socket.patch
	20-default-configs.patch
	30-conf-split-workers.patch
	40-$pkgname-$pkgver-unbundle-snowball.patch::https://gitweb.gentoo.org/repo/gentoo.git/plain/mail-filter/rspamd/files/rspamd-2.5-unbundle-snowball.patch
	"
builddir="$srcdir/$pkgname-${_commit:-$pkgver}"

case "$CARCH" in
	x86_64|aarch64|ppc64le)
		makedepends="$makedepends vectorscan-dev"
		_hm="ON" ;;
	*)
		_hm="OFF" ;;
esac

build() {
	#export CC=clang
	#export CXX=clang++
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCONFDIR=/etc/"$pkgname" \
		-DRUNDIR=/run/"$pkgname" \
		-DRSPAMD_USER="$pkgusers" \
		-DRSPAMD_GROUP="$pkggroups" \
		-DENABLE_REDIRECTOR=ON \
		-DENABLE_URL_INCLUDE=ON \
		-DENABLE_PCRE2=ON \
		-DENABLE_HYPERSCAN="$_hm" \
		-DENABLE_LUAJIT="$_luajit" \
		-DSYSTEM_FMT=ON \
		-DSYSTEM_XXHASH=ON \
		-DSYSTEM_ZSTD=ON \
		-GNinja \
		$CMAKE_CROSSOPTS
	cmake --build build --target all check
}

check() {
	case "$CARCH" in
	aarch64)
		# aarch64: FIXME: segfault
		return
		;;
	s390x|riscv64|x86)
		# s390x: attempt to call global 'require' (a nil value)
		# riscv64 x86: full tests need luajit
		./build/test/rspamd-test-cxx
		;;
	*)
		cmake --build build --target run-test
		;;
	esac
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	cd "$pkgdir"

	install -D usr/share/"$pkgname"/www/README.md \
		usr/share/doc/"$pkgname"/README.md
	rm usr/share/"$pkgname"/www/README.md

	install -Dm 644 "$srcdir/$pkgname".logrotated \
		etc/logrotate.d/"$pkgname"
	install -Dm 644 "$srcdir/$pkgname".confd \
		etc/conf.d/"$pkgname"
	install -Dm 755 "$srcdir/$pkgname".initd \
		etc/init.d/"$pkgname"

	install -dm 750 -o rspamd -g rspamd var/lib/"$pkgname"
	install -dm 750 -o rspamd -g rspamd var/log/"$pkgname"
	install -dm 755 -o rspamd -g rspamd etc/"$pkgname"/local.d/maps.d
	install -d etc/"$pkgname"/local.d etc/"$pkgname"/override.d
}

client() {
	pkgdesc="$pkgdesc (console client)"

	amove usr/bin/rspamc
}

libs() {
	pkgdesc="$pkgdesc (libraries)"

	amove usr/lib/"$pkgname"/*.so
}

utils() {
	pkgdesc="$pkgdesc (utilities)"
	depends="perl"

	amove usr/bin/"$pkgname"_stats
}

fuzzy() {
	pkgdesc="$pkgdesc (local fuzzy storage)"
	depends="$pkgname"

	amove etc/"$pkgname"/worker-fuzzy.* etc/"$pkgname"/modules.d/fuzzy_*
}

controller() {
	pkgdesc="$pkgdesc (controller web interface)"
	license="MIT"
	depends="$pkgname"

	amove usr/share/"$pkgname"/www etc/"$pkgname"/worker-controller.*
}

proxy() {
	pkgdesc="$pkgdesc (milter support)"
	depends="$pkgname"

	amove etc/"$pkgname"/worker-proxy.*
}

sha512sums="
1fc8341c5eeae38cc00a740b063e4fcd9869cd89f001ccc9eddd84641e5b67efa5b3f56d6019efca39d462deed08b1f8e460a5f48ccc80f8f5e2c5d7458ccd78  rspamd-3.5_git140323.tar.gz
2efe28575c40d1fba84b189bb872860e744400db80dce2f6330be6c6287fb3f46e6511284729b957488bf40bcb9b0952e26df9934f5f138334bd2766075c45cb  rspamd.logrotated
782e1126d32e450a1db0ac822c127b9a763f903093f200bdf603a6a0610a853671b94c89b0bb2d8ebdfb065e0cf62be51c1c7f451e8da34e25f252a276c2b0f3  rspamd.initd
a2003ef0c9d64a44480f59302864a2dfedcbe3a0047fcbb655408bc8aae9014b6ad0ddc6b64d4abeeb21bea0f86678afd30589ac8eed83e07ad7f87710e93702  rspamd.confd
6f828601ce460f2fd3b1c430974b719e36b0aa5600bd7074683fd646d99a4e76da35840b54c50416a9ae6d87babe39c5d463fc2ec05814e164a96d16f5da18b7  10-control_socket.patch
f0c907263b2c023501c5f3f9db60886e91a89f4b482d2baff70b5c9614bad7e3b97ff853de52382b87cca4bce9510997e0328ad998299ca2c2d0b79f2a07a13f  11-alpine-naming.patch
8801e3af59886847c25c87ca2f231cd9ededf35f376f9920967e72696fb017b1a4312f3a49017b52f65556bfe5c9f74a80405643afa32bb2906c38d39c5e1818  20-default-configs.patch
a8aefee649bf6630339d1d3f2bb20c25ca70b21a8eaa92951e926d0fd4525f1d4ac4cc7ea66ac2b15323cf02c93c759ddf7181502f0d71b21384ced9d88c008e  30-conf-split-workers.patch
5257921bdeb5febef99feebd03d291aad009a6b8a44c325305532bae7aec35456042b4813338f175c4a59aa80cce3178118d9e1e4bc5a500f42e5364c06a4dda  40-rspamd-3.5_git140323-unbundle-snowball.patch
"
