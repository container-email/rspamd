ARG DVER=latest
FROM docker.io/alpine:$DVER
LABEL maintainer="Duncan Bellamy <dunk@denkimushi.com>"
ARG APKVER

ENV dqsver 1.2.1

RUN apk update \
&& apk upgrade --available --no-cache \
&& apk add --no-cache --upgrade drill rspamd stunnel patch \
&& mkdir /run/rspamd && chown rspamd:rspamd /run/rspamd \
&& mkdir /run/stunnel && chown stunnel:stunnel /run/stunnel

WORKDIR /tmp
RUN wget -q https://github.com/spamhaus/rspamd-dqs/archive/v${dqsver}.tar.gz \
&& tar -xzf v${dqsver}.tar.gz \
&& mv rspamd-dqs-${dqsver}/3.x /etc/rspamd/rspamd-dqs \
&& wget -O rspamd.patch https://github.com/rspamd/rspamd/commit/1bfa26296e65514c00e18f856ef3297e7fefdd1c.patch \
&& patch -p2 -i /tmp/rspamd.patch -d /etc/rspamd \
&& rm -Rf ./*

WORKDIR /usr/local/bin
COPY --chmod=755 container-scripts/set-timezone.sh container-scripts/health-nc.sh entrypoint.sh update_*.sh ./

WORKDIR /etc/rspamd/local.d
COPY local.conf ./

WORKDIR /etc/rspamd/local.d/maps.orig
COPY --chown=rspamd:rspamd maps/* ./

COPY stunnel.conf /etc/stunnel/stunnel.conf

CMD [ "entrypoint.sh" ]
VOLUME /var/lib/rspamd /etc/rspamd/override.d /etc/rspamd/local.d/maps.d
EXPOSE 11332 11334

HEALTHCHECK --start-period=60s CMD wget -q -O - 127.0.0.1:11334/healthy | grep -q "success"
